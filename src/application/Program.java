package application;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import db.DB;
import db.DbException;

public class Program {

	public static void main(String[] args) {
		
		/* 
		 * SELECT
		 * 
		Connection conn = null;
		Statement  state = null;
		ResultSet  rs = null;
		
		try {
			conn = DB.getConnection();
			state = conn.createStatement();
			rs = state.executeQuery("select * from schools");
			
			while(rs.next()) {
				System.out.println(rs.getInt("id") + "- " + rs.getString("name"));
			}
			
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		} finally {
			
			DB.closeStatement(state);
			DB.closeConnection();
		}*/
		
		
		/*
		 * INSERT
		 * 
		Connection conn = null;
		PreparedStatement ps = null;
		Scanner sc = new Scanner(System.in);
		System.out.println("Escreva uma escola para inserir");
		String escola = sc.nextLine();
		int rowsAffected = 0;
		
		try {
			conn = DB.getConnection();
			
			ps = conn.prepareStatement("INSERT INTO schools (name) VALUES (?);");
			ps.setString(1, escola);
			
			rowsAffected = ps.executeUpdate();
			
			System.out.println("Feito, linhas novas: " + rowsAffected);
			
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		*/
				
		Connection conn = null;
		PreparedStatement ps = null;
		int rowsAffected = 0;
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Escreva um id de escola para atualizar:");
		String id = sc.nextLine();
		System.out.println("Escreva um novo nome de escola para atualizar:");
		String novoNome = sc.nextLine();
		
		try {
			conn = DB.getConnection();
			
			ps = conn.prepareStatement("UPDATE schools SET name = ?  WHERE (id = ?);");
			ps.setString(1, novoNome);
			ps.setString(2, id);
			
			rowsAffected = ps.executeUpdate();
			
			System.out.println("Feito, linhas novas: " + rowsAffected);
			
		} catch (SQLException e) {
			throw new DbException(e.getMessage());
		}
		

	}

}
